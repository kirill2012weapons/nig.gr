<?php

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>

<body>
<?php $this->beginBody() ?>

<div class="main">

    <header id="header"><strong class="slogan"><span class="holder">似顔絵グラフィックス結婚式の似顔絵ウェルカムボードやご両親贈呈用ボード、記念日・プレゼントボードなど、世界にひとつだけの似顔絵を</span></strong>
        <div class="holder">
            <h1 class="logo">
                <a href="https://nigaoe.graphics.vc/">似顔絵グラフィックス NIGAOE
                    GRAPHICS
                </a>
            </h1>
            <div class="frame">
                <ul class="list-push">
                    <li><a>銀行振込</a></li>
                    <li><a>代金引換</a></li>
                    <li><a>コンビニ決済</a></li>
                    <li><a>後払い決済</a></li>
                    <li><a>各種クレジット決済</a></li>
                </ul>
                <a href="/products/list.php" class="btn btn-order">注文する</a></div>
        </div>
        <nav>
            <ul>
                <li><a href="https://nigaoe.graphics.vc/" class="active">トップ</a></li>
                <li style="display: none;"><a href="/user_data/touch.php">タッチ一覧</a></li>
                <li><a href="/user_data/artist.php">似顔絵師一覧</a></li>
                <li><a href="/user_data/about.php">初めての方へ</a></li>
                <li><a href="/user_data/how_to_order.php">ご利用方法</a></li>
                <li><a href="/user_data/faq.php">よくある質問</a></li>
                <li><a href="/contact/index.php">お問い合わせ</a></li>
            </ul>
        </nav>
    </header>

    <?= $content ?>

    <footer id="footer">
        <div class="holder">
            <ul class="footer-list">
                <li><strong>ご利用方法</strong>
                    <ul>
                        <li><a href="/user_data/how_to_order.php">オーダーの方法</a></li>
                        <li><a href="/user_data/how_to_order02.php">サイズとお値段</a></li>
                        <li><a href="/user_data/how_to_order03.php">お支払い方法</a></li>
                        <li><a href="/user_data/how_to_order04.php">似顔絵のお届け日</a></li>
                        <li><a href="/user_data/how_to_order05.php">お写真の選び方</a></li>
                    </ul>
                </li>
                <li><strong>コンテンツ</strong>
                    <ul>
                        <li><a href="/user_data/about.php">似顔絵グラフィックスについて</a></li>
                        <li><a href="/products/list.php">オーダーする</a></li>
                        <li><a href="/user_data/faq.php">よくある質問</a></li>
                        <li><a href="/contact/index.php">お問い合わせ</a></li>
                    </ul>
                </li>
                <li><strong>シーンから選ぶ</strong>
                    <ul>
                        <li><a href="/user_data/welcome.php">ウェルカムボード</a></li>
                        <li><a href="/user_data/parents.php">ご両親進呈ボード</a></li>
                        <li><a href="/user_data/birthday.php">誕生日プレゼントボード</a></li>
                        <li><a href="/user_data/event.php">祝日 催事用ボード</a></li>
                        <li><a href="/user_data/option.php">オプション商品</a></li>
                    </ul>
                </li>
                <li><strong>似顔絵作家</strong>
                    <ul>
                        <li><a href="/products/detail.php?product_id=9">ZUGA</a></li>
                        <li><a href="/products/detail.php?product_id=13">ひでお</a></li>
                        <li><a href="/products/detail.php?product_id=14">赤石たつき</a></li>
                        <li><a href="/products/detail.php?product_id=15">ぴんくぶた</a></li>
                        <li><a href="/products/detail.php?product_id=7">ayuho</a></li>
                        <li><a href="/products/detail.php?product_id=24">藤井ゆき</a></li>
                        <li><a href="/products/detail.php?product_id=23">杉山和彦</a></li>
                        <li><a href="/products/detail.php?product_id=8">ビスマルク</a></li>
                        <li><a href="/products/detail.php?product_id=28">川上奈々(デジタルタッチ)</a></li>
                        <li><a href="/products/detail.php?product_id=27">川上奈々(アナログタッチ)</a></li>
                        <li><a href="/products/detail.php?product_id=25">知佐(chiSa)</a></li>
                        <li><a href="/products/detail.php?product_id=21">ao</a></li>
                        <li><a href="/products/detail.php?product_id=26">いっか</a></li>
                        <li><a href="/products/detail.php?product_id=39">かわのまほ(少女漫画タッチ)</a></li>
                        <li><a href="/products/detail.php?product_id=38">かわのまほ(ほんわかタッチ)</a></li>
                        <li><a href="/products/detail.php?product_id=37">かわのまほ(リアルタッチ)</a></li>
                        <li><a href="/products/detail.php?product_id=36">フロアーイーゼル</a></li>
                        <li><a href="/products/detail.php?product_id=35">卓上イーゼル</a></li>
                        <li><a href="/products/detail.php?product_id=34">ウェディングフレーム(アンティーク)</a></li>
                        <li><a href="/products/detail.php?product_id=33">ウェディングフレーム(エレガント)</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="add-nav">
                <li><a href="/user_data/term.php">ご利用規約</a></li>
                <li><a href="/order/index.php">法規表示</a></li>
                <li><a href="/guide/privacy.php">プライバシーポリシー</a></li>
                <li><a href="/contact/index.php">お問い合わせ</a></li>
            </ul>
            <p class="copyright">Copyright © 2005-2018 似顔絵グラフィックス All rights reserved</p></div>
    </footer>

</div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
