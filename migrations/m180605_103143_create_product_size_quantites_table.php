<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_size_quantites`.
 */
class m180605_103143_create_product_size_quantites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_size_quantites', [
            'id' => $this->primaryKey(),
            'modified_at' => $this->dateTime()->defaultValue(null),
            'created_at' => $this->dateTime()->notNull(),
            'product_id' => $this->integer(),
            'product_quantity_id' => $this->integer(),
            'product_size_id' => $this->integer(),
            'price' => $this->float(3)->notNull(),

        ]);

        //product_id
        $this->createIndex(
            'idx-product_size_quantites-product_id',
            'product_size_quantites',
            'product_id'
        );
        $this->addForeignKey(
            'fk-product_size_quantites-product_id',
            'product_size_quantites',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );

        //product_quantity_id
        $this->createIndex(
            'idx-product_size_quantites-product_quantity_id',
            'product_size_quantites',
            'product_quantity_id'
        );
        $this->addForeignKey(
            'fk-product_size_quantites-product_quantity_id',
            'product_size_quantites',
            'product_quantity_id',
            'product_quantites',
            'id',
            'CASCADE'
        );

        //product_size_id
        $this->createIndex(
            'idx-product_size_quantites-product_size_id',
            'product_size_quantites',
            'product_size_id'
        );
        $this->addForeignKey(
            'fk-product_size_quantites-product_size_id',
            'product_size_quantites',
            'product_size_id',
            'product_sizes',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //product_id
        $this->dropForeignKey(
            'fk-product_size_quantites-product_id',
            'product_size_quantites'
        );
        $this->dropIndex(
            'idx-product_size_quantites-product_id',
            'product_size_quantites'
        );

        //product_quantity_id
        $this->dropForeignKey(
            'fk-product_size_quantites-product_quantity_id',
            'product_size_quantites'
        );
        $this->dropIndex(
            'idx-product_size_quantites-product_quantity_id',
            'product_size_quantites'
        );

        //product_size_id
        $this->dropForeignKey(
            'fk-product_size_quantites-product_size_id',
            'product_size_quantites'
        );
        $this->dropIndex(
            'idx-product_size_quantites-product_size_id',
            'product_size_quantites'
        );

        $this->dropTable('product_size_quantites');
    }
}
