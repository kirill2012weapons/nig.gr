<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_product_baskets`.
 */
class m180605_113237_create_order_product_baskets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_product_baskets', [
            'id' => $this->primaryKey(),
            'modified_at' => $this->dateTime()->defaultValue(null),
            'created_at' => $this->dateTime()->notNull(),
            'order_information_basket_id' => $this->integer(),
            'product_size_quantity_id' => $this->integer(),
        ]);

        //order_information_basket_id
        $this->createIndex(
            'idx-order_product_baskets-order_information_basket_id',
            'order_product_baskets',
            'order_information_basket_id'
        );
        $this->addForeignKey(
            'fk-order_product_baskets-order_information_basket_id',
            'order_product_baskets',
            'order_information_basket_id',
            'order_information_baskets',
            'id',
            'CASCADE'
        );

        //product_size_quantity_id
        $this->createIndex(
            'idx-order_product_baskets-product_size_quantity_id',
            'order_product_baskets',
            'product_size_quantity_id'
        );
        $this->addForeignKey(
            'fk-order_product_baskets-product_size_quantity_id',
            'order_product_baskets',
            'product_size_quantity_id',
            'product_size_quantites',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        //order_information_basket_id
        $this->dropForeignKey(
            'fk-order_product_baskets-order_information_basket_id',
            'order_product_baskets'
        );
        $this->dropIndex(
            'idx-order_product_baskets-order_information_basket_id',
            'order_product_baskets'
        );

        //product_size_quantity_id
        $this->dropForeignKey(
            'fk-order_product_baskets-product_size_quantity_id',
            'order_product_baskets'
        );
        $this->dropIndex(
            'idx-order_product_baskets-product_size_quantity_id',
            'order_product_baskets'
        );

        $this->dropTable('order_product_baskets');
    }
}
