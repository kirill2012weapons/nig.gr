<?php

use yii\db\Migration;

/**
 * Handles the creation of table `free_frames`.
 */
class m180605_111206_create_free_frames_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('free_frames', [
            'id' => $this->primaryKey(),
            'modified_at' => $this->dateTime()->defaultValue(null),
            'created_at' => $this->dateTime()->notNull(),
            'name' => $this->string(45)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('free_frames');
    }
}
