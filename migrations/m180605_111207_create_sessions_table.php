<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sessions`.
 */
class m180605_111207_create_sessions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sessions', [
            'id' => $this->primaryKey(),
            'data' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sessions');
    }
}
