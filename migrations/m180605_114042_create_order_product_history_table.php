<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_product_history`.
 */
class m180605_114042_create_order_product_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_product_history', [
            'id' => $this->primaryKey(),
            'modified_at' => $this->dateTime()->defaultValue(null),
            'created_at' => $this->dateTime()->notNull(),
            'order_information_history_id' => $this->integer(),
            'product_size_quantity_id' => $this->integer(),
        ]);

        //order_information_history_id
        $this->createIndex(
            'idx-order_product_history-order_information_history_id',
            'order_product_history',
            'order_information_history_id'
        );
        $this->addForeignKey(
            'fk-order_product_history-order_information_history_id',
            'order_product_history',
            'order_information_history_id',
            'order_information_history',
            'id',
            'CASCADE'
        );

        //product_size_quantity_id
        $this->createIndex(
            'idx-order_product_history-product_size_quantity_id',
            'order_product_history',
            'product_size_quantity_id'
        );
        $this->addForeignKey(
            'fk-order_product_history-product_size_quantity_id',
            'order_product_history',
            'product_size_quantity_id',
            'product_size_quantites',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        //order_information_history_id
        $this->dropForeignKey(
            'fk-order_product_history-order_information_history_id',
            'order_product_history'
        );
        $this->dropIndex(
            'idx-order_product_history-order_information_history_id',
            'order_product_history'
        );

        //product_size_quantity_id
        $this->dropForeignKey(
            'fk-order_product_history-product_size_quantity_id',
            'order_product_history'
        );
        $this->dropIndex(
            'idx-order_product_history-product_size_quantity_id',
            'order_product_history'
        );

        $this->dropTable('order_product_history');
    }
}
