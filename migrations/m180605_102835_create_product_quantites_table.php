<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_quantites`.
 */
class m180605_102835_create_product_quantites_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_quantites', [
            'id' => $this->primaryKey(),
            'modified_at' => $this->dateTime()->defaultValue(null),
            'created_at' => $this->dateTime()->notNull(),
            'name' => $this->string(45)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product_quantites');
    }
}
