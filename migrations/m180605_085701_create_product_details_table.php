<?php

use yii\db\Migration;

class m180605_085701_create_product_details_table extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_details', [
            'id' => $this->primaryKey(),
            'modified_at' => $this->dateTime()->defaultValue(null),
            'created_at' => $this->dateTime()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'thumbnail' => $this->string(500)->notNull(),
            'images_json' => $this->text()->notNull(),
            'product_description' => $this->text()->notNull(),
        ]);

        $this->createIndex(
            'idx-product_details-product_id',
            'product_details',
            'product_id'
        );

        $this->addForeignKey(
            'fk-product_details-product_id',
            'product_details',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey(
            'fk-product_details-product_id',
            'product_details'
        );

        $this->dropIndex(
            'idx-product_details-product_id',
            'product_details'
        );

        $this->dropTable('product_details');

    }
}
