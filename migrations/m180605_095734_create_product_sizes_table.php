<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_sizes`.
 */
class m180605_095734_create_product_sizes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_sizes', [
            'id' => $this->primaryKey(),
            'modified_at' => $this->dateTime()->defaultValue(null),
            'created_at' => $this->dateTime()->notNull(),
            'name' => $this->string(100)->unique()->notNull(),
            'description' => $this->string(100)->notNull(),
            'img' => $this->string(500)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product_sizes');
    }
}
