<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_information_history`.
 */
class m180605_113814_create_order_information_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('order_information_history', [
            'id' => $this->primaryKey(),
            'modified_at' => $this->dateTime()->defaultValue(null),
            'created_at' => $this->dateTime()->notNull(),
            'free_frame_id' => $this->integer(),
            'name' => $this->string(50)->notNull(),
            'surname' => $this->string(50)->notNull(),
            'company_name' => $this->string(100),
            'postcode' => $this->string(8),
            'address_country' => $this->string(100),
            'address_street'=> $this->string(100),
            'phone' => $this->string(20),
            'email' => $this->string(100)->notNull(),
            'addition_text' => $this->text(),
        ]);

        //free_frame_id
        $this->createIndex(
            'idx-order_information_history-free_frame_id',
            'order_information_history',
            'free_frame_id'
        );
        $this->addForeignKey(
            'fk-order_information_history-free_frame_id',
            'order_information_history',
            'free_frame_id',
            'free_frames',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        //free_frame_id
        $this->dropForeignKey(
            'fk-order_information_history-free_frame_id',
            'order_information_history'
        );
        $this->dropIndex(
            'idx-order_information_history-free_frame_id',
            'order_information_history'
        );

        $this->dropTable('order_information_history');
    }
}
