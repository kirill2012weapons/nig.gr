<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ProductSize */
/* @var $form ActiveForm */
?>
<div>
    <ul class="list-about">
        <?php
        if (!empty($sizes)) echo '<h2 class="title-block">All Sizes</h2>';
        foreach ($sizes as $key => $value) :
        ?>
        <li>
            <div class="image" style="height: 50%; width: auto"><img src="<?= $value['img']; ?>" alt=" 似顔絵ウェルカムボード"></div>
            <div class="text-box">
                <h4><?= $value['name']; ?></h4>
                <p><?= $value['description']; ?></p>
            </div>

            <?php $form = ActiveForm::begin( [
                'action' => '/admin/product-sizes/delete',
                'options' => [
                    'style' => 'display: inline-block; vertical-align: top;'
                ]
            ]); ?>
            <div class="form-group">
                <?= Html::submitButton('Delete', ['class' => 'btn-orange', 'style' => 'background-color: #da0f0f; display: inline-block; margin-right: 15px;']) ?>
            </div>
            <input name="id" class="hidden" value="<?= $value['id']; ?>">
            <?php ActiveForm::end(); ?>

            <?php $form = ActiveForm::begin( [
                'action' => '/admin/product-sizes/change',
                'options' => [
                    'style' => 'display: inline-block; vertical-align: top;'
                ]
            ]); ?>
            <div class="form-group">
                <?= Html::submitButton('Change', ['class' => 'btn-orange', 'style' => 'background-color: #20abc1; display: block; margin-right: 15px;']) ?>
            </div>
            <input name="id" class="hidden" value="<?= $value['id']; ?>">
            <?php ActiveForm::end(); ?>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<h2 class="title-block">Add New Size</h2>
<div class="error-message">
    <ul>
        <?php
        if (Yii::$app->session->hasFlash('save')) {
            echo '<li style="color:green">' . Yii::$app->session->getFlash('save') . '</li>';
        } elseif (isset($errors) && !empty($errors)) {
            foreach ($errors as $key => $value) {
                echo '<li>' .  $value . '</li>';
            }
        }
        ?>
    </ul>
</div>
<div class="product-sizes-adds">

    <?php $form = ActiveForm::begin([
        'action' => '/admin/product-sizes/index',
    ]); ?>

    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'description')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn-orange']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- product-sizes-adds -->
