<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\ProductSize */
/* @var $form ActiveForm */
?>
<h2 class="title-block">Add New Size</h2>
<div class="error-message">
    <ul>
        <?php
        if (Yii::$app->session->hasFlash('save')) {
            echo '<li style="color:green">' . Yii::$app->session->getFlash('save') . '</li>';
        } elseif (isset($errors) && !empty($errors)) {
            foreach ($errors as $key => $value) {
                echo '<li>' .  $value . '</li>';
            }
        }
        ?>
    </ul>
</div>
<div class="product-sizes-adds">
    <div class="image" style="height: 50%; width: auto"><img src="<?= $model->img; ?>" alt=" 似顔絵ウェルカムボード"></div>
    <?php $form = ActiveForm::begin([
        'action' => '/admin/product-sizes/change',
    ]); ?>

    <?= $form->field($model, 'name',
        [
            'options' => [
                'value' => $model->name,
            ]
        ]
    ) ?>
    <?= $form->field($model, 'description',
        [
            'options' => [
                'value' => $model->name,
            ]
        ]
    )->textarea() ?>
    <input name="id" type="hidden" value="<?= $model->id; ?>">
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn-orange']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
