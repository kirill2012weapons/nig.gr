<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 05.06.2018
 * Time: 17:44
 */

namespace app\modules\admin\controllers;


use app\modules\admin\models\ProductSize;

class ProductSizesController extends \yii\web\Controller
{
    public function actionIndex() {

        $model = new ProductSize();
        $sizes = $model->getProductSize()->asArray()->all();

        $model->img = 'https://nigaoe.graphics.vc/upload/save_image/09111635_59b63cc2199fa.jpg';

        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                $sizes = $model->getProductSize()->asArray()->all();
                $this->redirect('/admin/product-sizes');
                return $this->render('index', ['model' => $model, 'save' => true, 'sizes' => $sizes]);
            }
        } elseif ($model->hasErrors()) {
            return $this->render('index', ['model' => $model, 'errors' => $model->getErrorSummary(true), 'sizes' => $sizes]);
        }
        return $this->render('index', ['model' => $model, 'errors' => $model->getErrorSummary(true), 'sizes' => $sizes]);

    }

    public function actionDelete() {

        $id = \Yii::$app->request->post('id');
        $size = ProductSize::findOne($id);
        $model = new ProductSize();

        if ($size->delete()) {
            $sizes = $model->getProductSize()->asArray()->all();
            $this->redirect('/admin/product-sizes');
            return $this->render('index', ['model' => $model, 'sizes' => $sizes]);
        }

    }

    public function actionChange() {
        $id = \Yii::$app->request->post('id');
        if (empty($id)) $this->redirect('/admin/product-sizes');
        $model = ProductSize::findOne($id);
        if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
            if ($model->update()) {
                return $this->redirect('/admin/product-sizes');
            }
        } elseif ($model->hasErrors()) {
            return $this->redirect('/admin/product-sizes');
        }

        return $this->render('change', ['model' => $model]);
    }

}
