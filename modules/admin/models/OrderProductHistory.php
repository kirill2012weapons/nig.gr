<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "order_product_history".
 *
 * @property int $id
 * @property string $modified_at
 * @property string $created_at
 * @property int $order_information_history_id
 * @property int $product_size_quantity_id
 *
 * @property OrderInformationHistory $orderInformationHistory
 * @property ProductSizeQuantites $productSizeQuantity
 */
class OrderProductHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_product_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modified_at', 'created_at'], 'safe'],
            [['created_at'], 'required'],
            [['order_information_history_id', 'product_size_quantity_id'], 'integer'],
            [['order_information_history_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderInformationHistory::className(), 'targetAttribute' => ['order_information_history_id' => 'id']],
            [['product_size_quantity_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSizeQuantites::className(), 'targetAttribute' => ['product_size_quantity_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'order_information_history_id' => 'Order Information History ID',
            'product_size_quantity_id' => 'Product Size Quantity ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderInformationHistory()
    {
        return $this->hasOne(OrderInformationHistory::className(), ['id' => 'order_information_history_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSizeQuantity()
    {
        return $this->hasOne(ProductSizeQuantites::className(), ['id' => 'product_size_quantity_id']);
    }
}
