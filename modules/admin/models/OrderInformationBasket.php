<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "order_information_baskets".
 *
 * @property int $id
 * @property string $modified_at
 * @property string $created_at
 * @property int $session_id
 * @property int $free_frame_id
 * @property string $name
 * @property string $surname
 * @property string $company_name
 * @property string $postcode
 * @property string $address_country
 * @property string $address_street
 * @property string $phone
 * @property string $email
 * @property string $addition_text
 *
 * @property FreeFrames $freeFrame
 * @property Sessions $session
 * @property OrderProductBaskets[] $orderProductBaskets
 */
class OrderInformationBasket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_information_baskets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modified_at', 'created_at'], 'safe'],
            [['created_at', 'name', 'surname', 'email'], 'required'],
            [['session_id', 'free_frame_id'], 'integer'],
            [['addition_text'], 'string'],
            [['name', 'surname'], 'string', 'max' => 50],
            [['company_name', 'address_country', 'address_street', 'email'], 'string', 'max' => 100],
            [['postcode'], 'string', 'max' => 8],
            [['phone'], 'string', 'max' => 20],
            [['free_frame_id'], 'exist', 'skipOnError' => true, 'targetClass' => FreeFrames::className(), 'targetAttribute' => ['free_frame_id' => 'id']],
            [['session_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sessions::className(), 'targetAttribute' => ['session_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'session_id' => 'Session ID',
            'free_frame_id' => 'Free Frame ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'company_name' => 'Company Name',
            'postcode' => 'Postcode',
            'address_country' => 'Address Country',
            'address_street' => 'Address Street',
            'phone' => 'Phone',
            'email' => 'Email',
            'addition_text' => 'Addition Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFreeFrame()
    {
        return $this->hasOne(FreeFrames::className(), ['id' => 'free_frame_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(Sessions::className(), ['id' => 'session_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProductBaskets()
    {
        return $this->hasMany(OrderProductBaskets::className(), ['order_information_basket_id' => 'id']);
    }
}
