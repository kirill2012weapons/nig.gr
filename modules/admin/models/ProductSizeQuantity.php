<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "product_size_quantites".
 *
 * @property int $id
 * @property string $modified_at
 * @property string $created_at
 * @property int $product_id
 * @property int $product_quantity_id
 * @property int $product_size_id
 * @property double $price
 *
 * @property OrderProductBaskets[] $orderProductBaskets
 * @property OrderProductHistory[] $orderProductHistories
 * @property Products $product
 * @property ProductQuantites $productQuantity
 * @property ProductSizes $productSize
 */
class ProductSizeQuantity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_size_quantites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modified_at', 'created_at'], 'safe'],
            [['created_at', 'price'], 'required'],
            [['product_id', 'product_quantity_id', 'product_size_id'], 'integer'],
            [['price'], 'number'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['product_quantity_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductQuantites::className(), 'targetAttribute' => ['product_quantity_id' => 'id']],
            [['product_size_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductSizes::className(), 'targetAttribute' => ['product_size_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'product_id' => 'Product ID',
            'product_quantity_id' => 'Product Quantity ID',
            'product_size_id' => 'Product Size ID',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProductBaskets()
    {
        return $this->hasMany(OrderProductBasket::className(), ['product_size_quantity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProductHistories()
    {
        return $this->hasMany(OrderProductHistory::className(), ['product_size_quantity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductQuantity()
    {
        return $this->hasOne(ProductQuantity::className(), ['id' => 'product_quantity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSize()
    {
        return $this->hasOne(ProductSize::className(), ['id' => 'product_size_id']);
    }
}
