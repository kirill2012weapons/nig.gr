<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "sessions".
 *
 * @property int $id
 * @property string $data
 *
 * @property OrderInformationBaskets[] $orderInformationBaskets
 */
class Session extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sessions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderInformationBaskets()
    {
        return $this->hasMany(OrderInformationBaskets::className(), ['session_id' => 'id']);
    }
}
