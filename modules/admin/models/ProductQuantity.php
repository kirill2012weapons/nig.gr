<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "product_quantites".
 *
 * @property int $id
 * @property string $modified_at
 * @property string $created_at
 * @property string $name
 *
 * @property ProductSizeQuantites[] $productSizeQuantites
 */
class ProductQuantity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_quantites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modified_at', 'created_at'], 'safe'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSizeQuantites()
    {
        return $this->hasMany(ProductSizeQuantity::className(), ['product_quantity_id' => 'id']);
    }
}
