<?php

namespace app\modules\admin\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "product_sizes".
 *
 * @property int $id
 * @property string $modified_at
 * @property string $created_at
 * @property string $name
 * @property string $description
 * @property string $img
 *
 * @property ProductSizeQuantites[] $productSizeQuantites
 */
class ProductSize extends \yii\db\ActiveRecord
{

    public function getProductSize() {
        return $this->find();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_sizes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['name', 'description', 'img'], 'required'],
            [['name', 'description'], 'string', 'max' => 100],
            [['img'], 'string', 'max' => 500],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'name' => 'Name',
            'description' => 'Description',
            'img' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductSizeQuantity()
    {
        return $this->hasMany(ProductSizeQuantity::className(), ['product_size_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'modified_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['modified_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }
}
