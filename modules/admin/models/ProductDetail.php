<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "product_details".
 *
 * @property int $id
 * @property string $modified_at
 * @property string $created_at
 * @property int $product_id
 * @property string $thumbnail
 * @property string $images_json
 * @property string $product_description
 *
 * @property Products $product
 */
class ProductDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modified_at', 'created_at'], 'safe'],
            [['created_at', 'product_id', 'thumbnail', 'images_json', 'product_description'], 'required'],
            [['product_id'], 'integer'],
            [['images_json', 'product_description'], 'string'],
            [['thumbnail'], 'string', 'max' => 500],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'product_id' => 'Product ID',
            'thumbnail' => 'Thumbnail',
            'images_json' => 'Images Json',
            'product_description' => 'Product Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
