<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "free_frames".
 *
 * @property int $id
 * @property string $modified_at
 * @property string $created_at
 * @property string $name
 *
 * @property OrderInformationBaskets[] $orderInformationBaskets
 * @property OrderInformationHistory[] $orderInformationHistories
 */
class FreeFrame extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'free_frames';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modified_at', 'created_at'], 'safe'],
            [['created_at', 'name'], 'required'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'modified_at' => 'Modified At',
            'created_at' => 'Created At',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderInformationBaskets()
    {
        return $this->hasMany(OrderInformationBaskets::className(), ['free_frame_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderInformationHistories()
    {
        return $this->hasMany(OrderInformationHistory::className(), ['free_frame_id' => 'id']);
    }
}
